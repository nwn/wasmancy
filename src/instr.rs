pub(crate) mod bytes {
    #![allow(unused)]

    pub const UNREACHABLE:   u8 = 0x00;
    pub const NOP:           u8 = 0x01;
    pub const BLOCK:         u8 = 0x02;
    pub const LOOP:          u8 = 0x03;
    pub const IF:            u8 = 0x04;
    pub const ELSE:          u8 = 0x05;

    pub const END:           u8 = 0x0b;
    pub const BRANCH:        u8 = 0x0c;
    pub const BRANCH_IF:     u8 = 0x0d;
    pub const BRANCH_TABLE:  u8 = 0x0e;
    pub const RETURN:        u8 = 0x0f;
    pub const CALL:          u8 = 0x10;
    pub const CALL_INDIRECT: u8 = 0x11;

    pub const DROP:          u8 = 0x1a;
    pub const SELECT:        u8 = 0x1b;
    pub const SELECT_TYPED:  u8 = 0x1c;

    pub const LOCAL_GET:     u8 = 0x20;
    pub const LOCAL_SET:     u8 = 0x21;
    pub const LOCAL_TEE:     u8 = 0x22;
    pub const GLOBAL_GET:    u8 = 0x23;
    pub const GLOBAL_SET:    u8 = 0x24;
    pub const TABLE_GET:     u8 = 0x25;
    pub const TABLE_SET:     u8 = 0x26;

    pub const I32_LOAD:      u8 = 0x28;
    pub const I64_LOAD:      u8 = 0x29;
    pub const F32_LOAD:      u8 = 0x2a;
    pub const F64_LOAD:      u8 = 0x2b;
    pub const I32_LOAD8_S:   u8 = 0x2c;
    pub const I32_LOAD8_U:   u8 = 0x2d;
    pub const I32_LOAD16_S:  u8 = 0x2e;
    pub const I32_LOAD16_U:  u8 = 0x2f;
    pub const I64_LOAD8_S:   u8 = 0x30;
    pub const I64_LOAD8_U:   u8 = 0x31;
    pub const I64_LOAD16_S:  u8 = 0x32;
    pub const I64_LOAD16_U:  u8 = 0x33;
    pub const I64_LOAD32_S:  u8 = 0x34;
    pub const I64_LOAD32_U:  u8 = 0x35;
    pub const I32_STORE:     u8 = 0x36;
    pub const I64_STORE:     u8 = 0x37;
    pub const F32_STORE:     u8 = 0x38;
    pub const F64_STORE:     u8 = 0x39;
    pub const I32_STORE8:    u8 = 0x3a;
    pub const I32_STORE16:   u8 = 0x3b;
    pub const I64_STORE8:    u8 = 0x3c;
    pub const I64_STORE16:   u8 = 0x3d;
    pub const I64_STORE32:   u8 = 0x3e;

    pub const MEMORY_SIZE:   u8 = 0x3f;
    pub const MEMORY_GROW:   u8 = 0x40;

    pub const I32_CONST:     u8 = 0x41;
    pub const I64_CONST:     u8 = 0x42;
    pub const F32_CONST:     u8 = 0x43;
    pub const F64_CONST:     u8 = 0x44;

    pub const I32_EQZ:       u8 = 0x45;
    pub const I32_EQ:        u8 = 0x46;
    pub const I32_NE:        u8 = 0x47;
    pub const I32_LT_S:      u8 = 0x48;
    pub const I32_LT_U:      u8 = 0x49;
    pub const I32_GT_S:      u8 = 0x4a;
    pub const I32_GT_U:      u8 = 0x4b;
    pub const I32_LE_S:      u8 = 0x4c;
    pub const I32_LE_U:      u8 = 0x4d;
    pub const I32_GE_S:      u8 = 0x4e;
    pub const I32_GE_U:      u8 = 0x4f;

    pub const I64_EQZ:       u8 = 0x50;
    pub const I64_EQ:        u8 = 0x51;
    pub const I64_NE:        u8 = 0x52;
    pub const I64_LT_S:      u8 = 0x53;
    pub const I64_LT_U:      u8 = 0x54;
    pub const I64_GT_S:      u8 = 0x55;
    pub const I64_GT_U:      u8 = 0x56;
    pub const I64_LE_S:      u8 = 0x57;
    pub const I64_LE_U:      u8 = 0x58;
    pub const I64_GE_S:      u8 = 0x59;
    pub const I64_GE_U:      u8 = 0x5a;

    pub const F32_EQ:        u8 = 0x5b;
    pub const F32_NE:        u8 = 0x5c;
    pub const F32_LT:        u8 = 0x5d;
    pub const F32_GT:        u8 = 0x5e;
    pub const F32_LE:        u8 = 0x5f;
    pub const F32_GE:        u8 = 0x60;

    pub const F64_EQ:        u8 = 0x61;
    pub const F64_NE:        u8 = 0x62;
    pub const F64_LT:        u8 = 0x63;
    pub const F64_GT:        u8 = 0x64;
    pub const F64_LE:        u8 = 0x65;
    pub const F64_GE:        u8 = 0x66;

    pub const I32_CLZ:       u8 = 0x67;
    pub const I32_CTZ:       u8 = 0x68;
    pub const I32_POPCNT:    u8 = 0x69;
    pub const I32_ADD:       u8 = 0x6a;
    pub const I32_SUB:       u8 = 0x6b;
    pub const I32_MUL:       u8 = 0x6c;
    pub const I32_DIV_S:     u8 = 0x6d;
    pub const I32_DIV_U:     u8 = 0x6e;
    pub const I32_REM_S:     u8 = 0x6f;
    pub const I32_REM_U:     u8 = 0x70;
    pub const I32_AND:       u8 = 0x71;
    pub const I32_OR:        u8 = 0x72;
    pub const I32_XOR:       u8 = 0x73;
    pub const I32_SHL:       u8 = 0x74;
    pub const I32_SHR_S:     u8 = 0x75;
    pub const I32_SHR_U:     u8 = 0x76;
    pub const I32_ROTL:      u8 = 0x77;
    pub const I32_ROTR:      u8 = 0x78;

    pub const I64_CLZ:       u8 = 0x79;
    pub const I64_CTZ:       u8 = 0x7a;
    pub const I64_POPCNT:    u8 = 0x7b;
    pub const I64_ADD:       u8 = 0x7c;
    pub const I64_SUB:       u8 = 0x7d;
    pub const I64_MUL:       u8 = 0x7e;
    pub const I64_DIV_S:     u8 = 0x7f;
    pub const I64_DIV_U:     u8 = 0x80;
    pub const I64_REM_S:     u8 = 0x81;
    pub const I64_REM_U:     u8 = 0x82;
    pub const I64_AND:       u8 = 0x83;
    pub const I64_OR:        u8 = 0x84;
    pub const I64_XOR:       u8 = 0x85;
    pub const I64_SHL:       u8 = 0x86;
    pub const I64_SHR_S:     u8 = 0x87;
    pub const I64_SHR_U:     u8 = 0x88;
    pub const I64_ROTL:      u8 = 0x89;
    pub const I64_ROTR:      u8 = 0x8a;

    pub const F32_ABS:       u8 = 0x8b;
    pub const F32_NEG:       u8 = 0x8c;
    pub const F32_CEIL:      u8 = 0x8d;
    pub const F32_FLOOR:     u8 = 0x8e;
    pub const F32_TRUNC:     u8 = 0x8f;
    pub const F32_NEAREST:   u8 = 0x90;
    pub const F32_SQRT:      u8 = 0x91;
    pub const F32_ADD:       u8 = 0x92;
    pub const F32_SUB:       u8 = 0x93;
    pub const F32_MUL:       u8 = 0x94;
    pub const F32_DIV:       u8 = 0x95;
    pub const F32_MIN:       u8 = 0x96;
    pub const F32_MAX:       u8 = 0x97;
    pub const F32_COPYSIGN:  u8 = 0x98;

    pub const F64_ABS:       u8 = 0x99;
    pub const F64_NEG:       u8 = 0x9a;
    pub const F64_CEIL:      u8 = 0x9b;
    pub const F64_FLOOR:     u8 = 0x9c;
    pub const F64_TRUNC:     u8 = 0x9d;
    pub const F64_NEAREST:   u8 = 0x9e;
    pub const F64_SQRT:      u8 = 0x9f;
    pub const F64_ADD:       u8 = 0xa0;
    pub const F64_SUB:       u8 = 0xa1;
    pub const F64_MUL:       u8 = 0xa2;
    pub const F64_DIV:       u8 = 0xa3;
    pub const F64_MIN:       u8 = 0xa4;
    pub const F64_MAX:       u8 = 0xa5;
    pub const F64_COPYSIGN:  u8 = 0xa6;

    pub const I32_WRAP_I64:        u8 = 0xa7;
    pub const I32_TRUNC_F32_S:     u8 = 0xa8;
    pub const I32_TRUNC_F32_U:     u8 = 0xa9;
    pub const I32_TRUNC_F64_S:     u8 = 0xaa;
    pub const I32_TRUNC_F64_U:     u8 = 0xab;
    pub const I64_EXTEND_I32_S:    u8 = 0xac;
    pub const I64_EXTEND_I32_U:    u8 = 0xad;
    pub const I64_TRUNC_F32_S:     u8 = 0xae;
    pub const I64_TRUNC_F32_U:     u8 = 0xaf;
    pub const I64_TRUNC_F64_S:     u8 = 0xb0;
    pub const I64_TRUNC_F64_U:     u8 = 0xb1;
    pub const F32_CONVERT_I32_S:   u8 = 0xb2;
    pub const F32_CONVERT_I32_U:   u8 = 0xb3;
    pub const F32_CONVERT_I64_S:   u8 = 0xb4;
    pub const F32_CONVERT_I64_U:   u8 = 0xb5;
    pub const F32_DEMOTE_F64:      u8 = 0xb6;
    pub const F64_CONVERT_I32_S:   u8 = 0xb7;
    pub const F64_CONVERT_I32_U:   u8 = 0xb8;
    pub const F64_CONVERT_I64_S:   u8 = 0xb9;
    pub const F64_CONVERT_I64_U:   u8 = 0xba;
    pub const F64_PROMOTE_F32:     u8 = 0xbb;
    pub const I32_REINTERPRET_F32: u8 = 0xbc;
    pub const I64_REINTERPRET_F64: u8 = 0xbd;
    pub const F32_REINTERPRET_I32: u8 = 0xbe;
    pub const F64_REINTERPRET_I64: u8 = 0xbf;

    pub const I32_EXTEND8_S:  u8 = 0xc0;
    pub const I32_EXTEND16_S: u8 = 0xc1;
    pub const I64_EXTEND8_S:  u8 = 0xc2;
    pub const I64_EXTEND16_S: u8 = 0xc3;
    pub const I64_EXTEND32_S: u8 = 0xc4;

    pub const REF_NULL:    u8 = 0xd0;
    pub const REF_IS_NULL: u8 = 0xd1;
    pub const REF_FUNC:    u8 = 0xd2;

    pub const EXTENDED: u8 = 0xfc;

    pub const I32_TRUNC_SAT_F32_S: u8 = 0x00;
    pub const I32_TRUNC_SAT_F32_U: u8 = 0x01;
    pub const I32_TRUNC_SAT_F64_S: u8 = 0x02;
    pub const I32_TRUNC_SAT_F64_U: u8 = 0x03;
    pub const I64_TRUNC_SAT_F32_S: u8 = 0x04;
    pub const I64_TRUNC_SAT_F32_U: u8 = 0x05;
    pub const I64_TRUNC_SAT_F64_S: u8 = 0x06;
    pub const I64_TRUNC_SAT_F64_U: u8 = 0x07;

    pub const MEMORY_INIT: u8 = 0x08;
    pub const DATA_DROP:   u8 = 0x09;
    pub const MEMORY_COPY: u8 = 0x0a;
    pub const MEMORY_FILL: u8 = 0x0b;
    pub const TABLE_INIT:  u8 = 0x0c;
    pub const ELEM_DROP:   u8 = 0x0d;
    pub const TABLE_COPY:  u8 = 0x0e;
    pub const TABLE_GROW:  u8 = 0x0f;
    pub const TABLE_SIZE:  u8 = 0x10;
    pub const TABLE_FILL:  u8 = 0x11;

    pub const EMPTY_BLOCK: u8 = 0x40;

    macro_rules! LOAD_STORE {
        () => { $crate::instr::bytes::I32_LOAD ..= $crate::instr::bytes::I64_STORE32 };
    }
    macro_rules! ARITHMETIC {
        () => { $crate::instr::bytes::I32_EQZ ..= $crate::instr::bytes::I64_EXTEND32_S };
    }
    macro_rules! TRUNC_SAT {
        () => { $crate::instr::bytes::I32_TRUNC_SAT_F32_S ..= $crate::instr::bytes::I64_TRUNC_SAT_F64_U };
    }
    macro_rules! RESERVED { () => {
        | 0x06 ..= 0x0a
        | 0x12 ..= 0x19
        | 0x1d ..= 0x1f
        | 0x27
        | 0xc5 ..= 0xcf
        | 0xd3 ..= 0xfb
        | 0xfd ..= 0xff
    }; }
    macro_rules! EXTENDED_RESERVED { () => {
        | 0x12 ..= 0xff
    }; }

    pub(crate) use LOAD_STORE;
    pub(crate) use ARITHMETIC;
    pub(crate) use TRUNC_SAT;
    pub(crate) use RESERVED;
    pub(crate) use EXTENDED_RESERVED;
}

#[cfg(never)]
fn validate_instructions(bytes: &mut &[u8]) {
    let mut next = || bytes.split_first().map(|(first, rest)| {
        *bytes = rest;
        *first
    });

    while let Some(byte) = next() {
        match byte {
            0x00 => "Unreachable",
            0x01 => "No-op",
            0x02 => "Block (type, instructions..., 0x0b)",
            0x03 => "Loop (type, instructions..., 0x0b)",
            0x04 => "If (type, instructions..., 0x0b)",
            0x04 => "If-else (type, instructions..., 0x05, instructions..., 0x0b)",

            0x05..=0x0b => "Reserved",

            0x0c => "Break (label)",
            0x0d => "Break-if (label)",
            0x0e => "Break table (labels..., label_idx)",
            0x0f => "Return",
            0x10 => "Call (func_idx)",
            0x11 => "Call-indirect (type_idx, 0x00)",

            0x12..=0x19 => "Reserved",

            0x1a => "Drop",
            0x1b => "Select",

            0x1c..=0x1f => "Reserved",

            0x20 => "Local-get (local_idx)",
            0x21 => "Local-set (local_idx)",
            0x22 => "Local-tee (local_idx)",
            0x23 => "Global-get (global_idx)",
            0x24 => "Global-set (global_idx)",

            0x25..=0x27 => "Reserved",

            0x28 => "Load-i32 (mem_arg)",
            0x29 => "Load-i64 (mem_arg)",
            0x2a => "Load-f32 (mem_arg)",
            0x2b => "Load-f64 (mem_arg)",
            0x2c => "Load-i32-s8 (mem_arg)",
            0x2d => "Load-i32-u8 (mem_arg)",
            0x2e => "Load-i32-s16 (mem_arg)",
            0x2f => "Load-i32-u16 (mem_arg)",
            0x30 => "Load-i64-s8 (mem_arg)",
            0x31 => "Load-i64-u8 (mem_arg)",
            0x32 => "Load-i64-s16 (mem_arg)",
            0x33 => "Load-i64-u16 (mem_arg)",
            0x34 => "Load-i64-s32 (mem_arg)",
            0x35 => "Load-i64-u32 (mem_arg)",
            0x36 => "Store-i32 (mem_arg)",
            0x37 => "Store-i64 (mem_arg)",
            0x38 => "Store-f32 (mem_arg)",
            0x39 => "Store-f64 (mem_arg)",
            0x3a => "Store-i32-8 (mem_arg)",
            0x3b => "Store-i32-16 (mem_arg)",
            0x3c => "Store-i64-8 (mem_arg)",
            0x3d => "Store-i64-16 (mem_arg)",
            0x3e => "Store-i64-32 (mem_arg)",
            0x3f => "Memory-size (0x00)",
            0x40 => "Memory-grow (0x00)",
            0x41 => "Const-i32 (i32)",
            0x42 => "Const-i64 (i64)",
            0x43 => "Const-f32 (f32)",
            0x44 => "Const-f64 (f64)",

            0x45 => "Eqz-i32",
            0x46 => "Eq-i32",
            0x47 => "Ne-i32",
            0x48 => "Lt-s32",
            0x49 => "Lt-u32",
            0x4a => "Gt-s32",
            0x4b => "Gt-u32",
            0x4c => "Le-s32",
            0x4d => "Le-u32",
            0x4e => "Ge-s32",
            0x4f => "Ge-u32",

            0x50 => "Eqz-i64",
            0x51 => "Eq-i64",
            0x52 => "Ne-i64",
            0x53 => "Lt-s64",
            0x54 => "Lt-u64",
            0x55 => "Gt-s64",
            0x56 => "Gt-u64",
            0x57 => "Le-s64",
            0x58 => "Le-u64",
            0x59 => "Ge-s64",
            0x5a => "Ge-u64",

            0x5b => "Eq-f32",
            0x5c => "Ne-f32",
            0x5d => "Lt-f32",
            0x5e => "Gt-f32",
            0x5f => "Le-f32",
            0x60 => "Ge-f32",

            0x61 => "Eq-f64",
            0x62 => "Ne-f64",
            0x63 => "Lt-f64",
            0x64 => "Gt-f64",
            0x65 => "Le-f64",
            0x66 => "Ge-f64",

            0x67 => "Clz-i32",
            0x68 => "Ctz-i32",
            0x69 => "Popcnt-i32",
            0x6a => "Add-i32",
            0x6b => "Sub-i32",
            0x6c => "Mul-i32",
            0x6d => "Div-s32",
            0x6e => "Div-u32",
            0x6f => "Rem-s32",
            0x70 => "Rem-u32",
            0x71 => "And-i32",
            0x72 => "Or-i32",
            0x73 => "Xor-i32",
            0x74 => "Shl-i32",
            0x75 => "Shr-s32",
            0x76 => "Shr-u32",
            0x77 => "Rotl-i32",
            0x78 => "Rotr-i32",

            0x79 => "Clz-i64",
            0x7a => "Ctz-i64",
            0x7b => "Popcnt-i64",
            0x7c => "Add-i64",
            0x7d => "Sub-i64",
            0x7e => "Mul-i64",
            0x7f => "Div-s64",
            0x80 => "Div-u64",
            0x81 => "Rem-s64",
            0x82 => "Rem-u64",
            0x83 => "And-i64",
            0x84 => "Or-i64",
            0x85 => "Xor-i64",
            0x86 => "Shl-i64",
            0x87 => "Shr-s64",
            0x88 => "Shr-u64",
            0x89 => "Rotl-i64",
            0x8a => "Rotr-i64",

            0x8b => "Abs-f32",
            0x8c => "Neg-f32",
            0x8d => "Ceil-f32",
            0x8e => "Floor-f32",
            0x8f => "Trunc-f32",
            0x90 => "Nearest-f32",
            0x91 => "Sqrt-f32",
            0x92 => "Add-f32",
            0x93 => "Sub-f32",
            0x94 => "Mul-f32",
            0x95 => "Div-f32",
            0x96 => "Min-f32",
            0x97 => "Max-f32",
            0x98 => "Copysign-f32",

            0x99 => "Abs-f64",
            0x9a => "Neg-f64",
            0x9b => "Ceil-f64",
            0x9c => "Floor-f64",
            0x9d => "Trunc-f64",
            0x9e => "Nearest-f64",
            0x9f => "Sqrt-f64",
            0xa0 => "Add-f64",
            0xa1 => "Sub-f64",
            0xa2 => "Mul-f64",
            0xa3 => "Div-f64",
            0xa4 => "Min-f64",
            0xa5 => "Max-f64",
            0xa6 => "Copysign-f64",

            0xa7 => "Wrap-i64-i32",
            0xa8 => "Trunc-f32-s32",
            0xa9 => "Trunc-f32-u32",
            0xaa => "Trunc-f64-s32",
            0xab => "Trunc-f64-u32",
            0xac => "Extend-i32-s64",
            0xad => "Extend-i32-u64",
            0xae => "Trunc-f32-s64",
            0xaf => "Trunc-f32-u64",
            0xb0 => "Trunc-f64-s64",
            0xb1 => "Trunc-f64-u64",
            0xb2 => "Convert-s32-f32",
            0xb3 => "Convert-u32-f32",
            0xb4 => "Convert-s64-f32",
            0xb5 => "Convert-u64-f32",
            0xb6 => "Demote-f64-f32",
            0xb7 => "Convert-s32-f64",
            0xb8 => "Convert-u32-f64",
            0xb9 => "Convert-s64-f64",
            0xba => "Convert-u64-f64",
            0xbb => "Promote-f32-f64",
            0xbc => "Reinterpret-f32-i32",
            0xbd => "Reinterpret-f64-i64",
            0xbe => "Reinterpret-i32-f32",
            0xbf => "Reinterpret-i64-f64",

            0xc0 => "Extend-s8-i32",
            0xc1 => "Extend-s16-i32",
            0xc2 => "Extend-s8-i64",
            0xc3 => "Extend-s16-i64",
            0xc4 => "Extend-s32-i64",

            0xc5..=0xfb => "Reserved",

            0xfc => "Trunc-sat-f32-s32 (0_u32)",
            0xfc => "Trunc-sat-f32-u32 (1_u32)",
            0xfc => "Trunc-sat-f64-s32 (2_u32)",
            0xfc => "Trunc-sat-f64-u32 (3_u32)",
            0xfc => "Trunc-sat-f32-s64 (4_u32)",
            0xfc => "Trunc-sat-f32-u64 (5_u32)",
            0xfc => "Trunc-sat-f64-s64 (6_u32)",
            0xfc => "Trunc-sat-f64-u64 (7_u32)",

            0xfd..=0xff => "Reserved",
        };
    }
}
