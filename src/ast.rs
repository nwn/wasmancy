use crate::Decode;
use wasmancy_macros::Decode;

pub struct Int32(pub u32);
pub struct Int64(pub u64);

#[derive(Copy, Clone)]
pub enum NumType {
    I32 = 0x7f,
    I64 = 0x7e,
    F32 = 0x7d,
    F64 = 0x7c,
}

#[derive(Copy, Clone)]
pub enum RefType {
    FunctionRef = 0x70,
    ExternRef = 0x6f,
}

#[derive(Copy, Clone)]
pub enum ValueType {
    Num(NumType),
    Ref(RefType),
}

#[derive(Decode)]
pub struct ResultType(pub Vec<ValueType>);

pub struct FunctionType {
    pub param_type: ResultType,
    pub result_type: ResultType,
}

pub struct Limits {
    pub min: u32,
    pub max: Option<u32>,
}

#[derive(Decode)]
pub struct MemoryType(pub Limits);

#[derive(Decode)]
pub struct TableType {
    pub elem_type: RefType,
    pub limits: Limits,
}

#[derive(Decode)]
pub struct GlobalType {
    pub val_type: ValueType,
    pub mutability: bool,
}

pub enum ExternalType {
    Function(FunctionType),
    Table(TableType),
    Memory(MemoryType),
    Global(GlobalType),
}

#[derive(Decode)]
pub struct TypeIdx(pub u32);
#[derive(Decode)]
pub struct FunctionIdx(pub u32);
#[derive(Decode)]
pub struct TableIdx(pub u32);
#[derive(Decode)]
pub struct MemoryIdx(pub u32);
#[derive(Decode)]
pub struct GlobalIdx(pub u32);
#[derive(Decode)]
pub struct ElementIdx(pub u32);
#[derive(Decode)]
pub struct DataIdx(pub u32);
#[derive(Decode)]
pub struct LocalIdx(pub u32);
#[derive(Decode)]
pub struct LabelIdx(pub u32);

#[derive(Decode)]
pub struct Function {
    pub typ: TypeIdx,
    pub locals: Vec<ValueType>,
    pub body: Expr,
}
#[derive(Decode)]
pub struct Table {
    pub typ: TableType,
}
#[derive(Decode)]
pub struct Memory {
    pub typ: MemoryType,
}
#[derive(Decode)]
pub struct Global {
    pub typ: GlobalType,
    pub init: Expr,
}
#[derive(Decode)]
pub struct ElementSegment {
    pub table: TableIdx,
    pub offset: Expr,
    pub init: Vec<FunctionIdx>,
}
#[derive(Decode)]
pub struct DataSegment {
    pub data: MemoryIdx,
    pub offset: Expr,
    pub init: Vec<u8>,
}
#[derive(Decode)]
pub struct StartFunction {
    pub func: FunctionIdx,
}
#[derive(Decode)]
pub struct Export {
    pub name: String,
    pub desc: ExportDescriptor,
}
pub enum ExportDescriptor {
    Function(FunctionIdx),
    Table(TableIdx),
    Memory(MemoryIdx),
    Global(GlobalIdx),
}
#[derive(Decode)]
pub struct Import {
    pub module: String,
    pub name: String,
    pub desc: ImportDescriptor,
}
pub enum ImportDescriptor {
    Function(TypeIdx),
    Table(TableType),
    Memory(MemoryType),
    Global(GlobalType),
}

pub struct Expr {
    pub instructions: Vec<u8>,
}

pub struct Module {
    pub types: Vec<FunctionType>,
    pub functions: Vec<Function>,
    pub tables: Vec<Table>,
    pub memories: Vec<Memory>,
    pub globals: Vec<Global>,
    pub elem: Vec<ElementSegment>,
    pub data: Vec<DataSegment>,
    pub start: Option<FunctionIdx>,
    pub imports: Vec<Import>,
    pub exports: Vec<Export>,
}
