mod ast;
mod bin;
mod instr;

pub use bin::Decode;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
