use crate::ast::*;
use wasmancy_macros::Decode;
use std::convert::TryInto;

pub trait Decode: Sized {
    fn decode(bytes: &mut &[u8]) -> Result<Self, ()>;
}

impl Decode for u8 {
    fn decode(bytes: &mut &[u8]) -> Result<u8, ()> {
        let (&first, rest) = bytes.split_first().ok_or(())?;
        *bytes = rest;
        Ok(first)
    }
}

impl Decode for bool {
    fn decode(bytes: &mut &[u8]) -> Result<bool, ()> {
        Ok(match u8::decode(bytes)? {
            0 => false,
            1 => true,
            _ => return Err(()),
        })
    }
}

fn decode_leb128_32(bytes: &mut &[u8]) -> Result<(u32, u8), ()> {
    let mut value = 0;
    let mut len = 0;
    let mut shift = 0;
    loop {
        if shift >= 32 {
            return Err(());
        }
        let byte = bytes.get(len).ok_or(())?;
        if shift == 28 && (byte & 0xf0) != 0 {
            return Err(());
        }
        let bits = ((byte & 0x7f) as u32) << shift;
        value |= bits;
        len += 1;
        shift += 7;
        if (byte & 0x80) == 0 {
            break;
        }
    }
    *bytes = &bytes[len..];
    Ok((value, shift))
}

fn decode_leb128_64(bytes: &mut &[u8]) -> Result<(u64, u8), ()> {
    let mut value = 0;
    let mut len = 0;
    let mut shift = 0;
    loop {
        if shift >= 64 {
            return Err(());
        }
        let byte = bytes.get(len).ok_or(())?;
        if shift == 63 && (byte & 0xfe) != 0 {
            return Err(());
        }
        let bits = ((byte & 0x7f) as u64) << shift;
        value |= bits;
        len += 1;
        shift += 7;
        if (byte & 0x80) == 0 {
            break;
        }
    }
    *bytes = &bytes[len..];
    Ok((value, shift))
}

impl Decode for u32 {
    fn decode(bytes: &mut &[u8]) -> Result<u32, ()> {
        decode_leb128_32(bytes).map(|(value, _)| value)
    }
}

impl Decode for u64 {
    fn decode(bytes: &mut &[u8]) -> Result<u64, ()> {
        decode_leb128_64(bytes).map(|(value, _)| value)
    }
}

impl Decode for i32 {
    fn decode(bytes: &mut &[u8]) -> Result<i32, ()> {
        decode_leb128_32(bytes).map(|(mut value, bit_len)| {
            if (bit_len < 32) && (value & (1 << (bit_len - 1))) != 0 {
                value |= !0 << bit_len; // Sign extend
            }
            value as i32
        })
    }
}

impl Decode for i64 {
    fn decode(bytes: &mut &[u8]) -> Result<i64, ()> {
        decode_leb128_64(bytes).map(|(mut value, bit_len)| {
            if (bit_len < 64) && (value & (1 << (bit_len - 1))) != 0 {
                value |= !0 << bit_len; // Sign extend
            }
            value as i64
        })
    }
}

impl Decode for Int32 {
    fn decode(bytes: &mut &[u8]) -> Result<Int32, ()> {
        Ok(Int32(i32::decode(bytes)? as u32))
    }
}

impl Decode for Int64 {
    fn decode(bytes: &mut &[u8]) -> Result<Int64, ()> {
        Ok(Int64(i64::decode(bytes)? as u64))
    }
}

impl Decode for f32 {
    fn decode(bytes: &mut &[u8]) -> Result<f32, ()> {
        let raw_bytes = bytes.get(..4).ok_or(())?;
        let raw_bytes = raw_bytes.try_into().expect("slice must have length 4");
        let value = f32::from_le_bytes(raw_bytes);
        *bytes = &bytes[4..];
        Ok(value)
    }
}

impl Decode for f64 {
    fn decode(bytes: &mut &[u8]) -> Result<f64, ()> {
        let raw_bytes = bytes.get(..8).ok_or(())?;
        let raw_bytes = raw_bytes.try_into().expect("slice must have length 8");
        let value = f64::from_le_bytes(raw_bytes);
        *bytes = &bytes[8..];
        Ok(value)
    }
}

impl Decode for NumType {
    fn decode(bytes: &mut &[u8]) -> Result<NumType, ()> {
        Ok(match u8::decode(bytes)? {
            0x7f => NumType::I32,
            0x7e => NumType::I64,
            0x7d => NumType::F32,
            0x7c => NumType::F64,
            _ => return Err(()),
        })
    }
}

impl Decode for RefType {
    fn decode(bytes: &mut &[u8]) -> Result<RefType, ()> {
        Ok(match u8::decode(bytes)? {
            0x70 => RefType::FunctionRef,
            0x6f => RefType::ExternRef,
            _ => return Err(()),
        })
    }
}

impl Decode for ValueType {
    fn decode(bytes: &mut &[u8]) -> Result<ValueType, ()> {
        Ok(match u8::decode(bytes)? {
            0x7f => ValueType::Num(NumType::I32),
            0x7e => ValueType::Num(NumType::I64),
            0x7d => ValueType::Num(NumType::F32),
            0x7c => ValueType::Num(NumType::F64),
            0x70 => ValueType::Ref(RefType::FunctionRef),
            0x6f => ValueType::Ref(RefType::ExternRef),
            _ => return Err(()),
        })
    }
}

impl Decode for FunctionType {
    fn decode(bytes: &mut &[u8]) -> Result<FunctionType, ()> {
        if u8::decode(bytes)? == 0x60 {
            Ok(FunctionType {
                param_type: ResultType::decode(bytes)?,
                result_type: ResultType::decode(bytes)?,
            })
        } else {
            Err(())
        }
    }
}

impl Decode for Limits {
    fn decode(bytes: &mut &[u8]) -> Result<Limits, ()> {
        let has_max = bool::decode(bytes)?;
        let min = u32::decode(bytes)?;
        let max = if has_max {
            Some(u32::decode(bytes)?)
        } else {
            None
        };
        Ok(Limits { min, max })
    }
}

impl Decode for ExportDescriptor {
    fn decode(bytes: &mut &[u8]) -> Result<ExportDescriptor, ()> {
        Ok(match u8::decode(bytes)? {
            0x00 => ExportDescriptor::Function(Decode::decode(bytes)?),
            0x01 => ExportDescriptor::Table(Decode::decode(bytes)?),
            0x02 => ExportDescriptor::Memory(Decode::decode(bytes)?),
            0x03 => ExportDescriptor::Global(Decode::decode(bytes)?),
            _ => return Err(()),
        })
    }
}

impl Decode for ImportDescriptor {
    fn decode(bytes: &mut &[u8]) -> Result<ImportDescriptor, ()> {
        Ok(match u8::decode(bytes)? {
            0x00 => ImportDescriptor::Function(Decode::decode(bytes)?),
            0x01 => ImportDescriptor::Table(Decode::decode(bytes)?),
            0x02 => ImportDescriptor::Memory(Decode::decode(bytes)?),
            0x03 => ImportDescriptor::Global(Decode::decode(bytes)?),
            _ => return Err(()),
        })
    }
}

impl<Elem: Decode> Decode for Vec<Elem> {
    fn decode(bytes: &mut &[u8]) -> Result<Vec<Elem>, ()> {
        let len = u32::decode(bytes)?;
        let mut vec = Vec::with_capacity(len as usize);
        for _ in 0..len {
            vec.push(Elem::decode(bytes)?);
        }
        Ok(vec)
    }
}

impl<Elem: Decode> Decode for Option<Elem> {
    fn decode(bytes: &mut &[u8]) -> Result<Option<Elem>, ()> {
        let mut test_bytes = *bytes;
        if let Ok(elem) = Elem::decode(&mut test_bytes) {
            *bytes = test_bytes;
            Ok(Some(elem))
        } else {
            Ok(None)
        }
    }
}

impl Decode for String {
    fn decode(bytes: &mut &[u8]) -> Result<String, ()> {
        String::from_utf8(Vec::<u8>::decode(bytes)?).map_err(|_| ())
    }
}

struct RawSection<'s> {
    id: u8,
    contents: &'s [u8],
}

fn extract_section<'s>(bytes: &mut &'s[u8]) -> Result<Option<RawSection<'s>>, ()> {
    if let Some(id @ 0..=12) = bytes.first().copied() {
        assert_eq!(id, u8::decode(bytes)?);
        let content_len = u32::decode(bytes)? as usize;
        if content_len >= bytes.len() {
            return Err(());
        }
        let (contents, rest) = bytes.split_at(content_len);
        *bytes = rest;

        Ok(Some(RawSection {
            id,
            contents,
        }))
    } else if bytes.is_empty() {
        Ok(None)
    } else {
        Err(())
    }
}

fn decode_section<Type: Decode + Default>(bytes: &mut &[u8], section_id: u8) -> Result<Type, ()> {
    if let Some(RawSection { id, mut contents }) = extract_section(bytes)? {
        if id != section_id {
            return Err(());
        }
        let result = Type::decode(&mut contents)?;
        if !contents.is_empty() {
            return Err(());
        }
        Ok(result)
    } else {
        Ok(Type::default())
    }
}

fn skip_custom_sections(bytes: &mut &[u8]) -> Result<(), ()> {
    while bytes.starts_with(&[0]) {
        let RawSection { id, mut contents } = extract_section(bytes)?.unwrap();
        assert_eq!(id, 0);
        let _name = String::decode(&mut contents)?;
    }
    Ok(())
}

fn scan_expr(bytes: &[u8], mut pos: usize) -> Result<(usize, u8), ()> {
    use crate::instr::bytes::*;

    fn scan_decode<Type: Decode>(bytes: &[u8]) -> Result<usize, ()> {
        let mut scanning_bytes = bytes;
        Type::decode(&mut scanning_bytes)?;
        Ok(bytes.len() - scanning_bytes.len())
    }

    fn scan_block_type(bytes: &[u8]) -> Result<usize, ()> {
        Ok(if bytes.get(0) == Some(&EMPTY_BLOCK) {
            1
        } else if let Ok(new_pos) = scan_decode::<ValueType>(bytes) {
            new_pos
        } else {
            // TODO: Handle s33
            return Err(());
        })
    }

    fn expect_end((pos, ending): (usize, u8)) -> Result<usize, ()> {
        if ending == END {
            Ok(pos)
        } else {
            Err(())
        }
    }

    loop {
        let opcode = *bytes.get(pos).ok_or(())?;
        pos += 1;
        match opcode {
            END => return Ok((pos, END)),
            ELSE => return Ok((pos, ELSE)),
            UNREACHABLE | NOP | RETURN => (),
            BLOCK | LOOP => {
                pos += scan_block_type(&bytes[pos..])?;
                pos = expect_end(scan_expr(bytes, pos)?)?;
            }
            IF => {
                pos += scan_block_type(&bytes[pos..])?;
                let (new_pos, ending) = scan_expr(bytes, pos)?;
                pos = new_pos;
                if ending == ELSE {
                    pos = expect_end(scan_expr(bytes, pos)?)?;
                }
            }
            BRANCH | BRANCH_IF => {
                pos += scan_decode::<LabelIdx>(&bytes[pos..])?;
            }
            BRANCH_TABLE => {
                pos += scan_decode::<Vec<LabelIdx>>(&bytes[pos..])?;
                pos += scan_decode::<LabelIdx>(&bytes[pos..])?;
            }
            CALL => {
                pos += scan_decode::<FunctionIdx>(&bytes[pos..])?;
            }
            CALL_INDIRECT => {
                pos += scan_decode::<TypeIdx>(&bytes[pos..])?;
                pos += scan_decode::<TableIdx>(&bytes[pos..])?;
            }
            DROP | SELECT => (),
            SELECT_TYPED => {
                pos += scan_decode::<Vec<ValueType>>(&bytes[pos..])?;
            }
            LOCAL_GET | LOCAL_SET | LOCAL_TEE => {
                pos += scan_decode::<LocalIdx>(&bytes[pos..])?;
            }
            GLOBAL_GET | GLOBAL_SET => {
                pos += scan_decode::<GlobalIdx>(&bytes[pos..])?;
            }
            TABLE_GET | TABLE_SET => {
                pos += scan_decode::<TableIdx>(&bytes[pos..])?;
            }
            LOAD_STORE!() => {
                pos += scan_decode::<u32>(&bytes[pos..])?;
                pos += scan_decode::<u32>(&bytes[pos..])?;
            }
            MEMORY_SIZE | MEMORY_GROW => {
                pos += scan_decode::<u8>(&bytes[pos..])?;
            }
            I32_CONST => {
                pos += scan_decode::<Int32>(&bytes[pos..])?;
            }
            I64_CONST => {
                pos += scan_decode::<Int64>(&bytes[pos..])?;
            }
            F32_CONST => {
                pos += scan_decode::<f32>(&bytes[pos..])?;
            }
            F64_CONST => {
                pos += scan_decode::<f64>(&bytes[pos..])?;
            }
            ARITHMETIC!() => (),
            REF_NULL => {
                pos += scan_decode::<RefType>(&bytes[pos..])?;
            }
            REF_IS_NULL => (),
            REF_FUNC => {
                pos += scan_decode::<FunctionIdx>(&bytes[pos..])?;
            }
            EXTENDED => {
                let opcode = *bytes.get(pos).ok_or(())?;
                pos += 1;
                match opcode {
                    TRUNC_SAT!() => (),
                    MEMORY_INIT => {
                        pos += scan_decode::<DataIdx>(&bytes[pos..])?;
                        pos += scan_decode::<u8>(&bytes[pos..])?;
                    }
                    DATA_DROP => {
                        pos += scan_decode::<DataIdx>(&bytes[pos..])?;
                    }
                    MEMORY_COPY => {
                        pos += scan_decode::<u8>(&bytes[pos..])?;
                        pos += scan_decode::<u8>(&bytes[pos..])?;
                    }
                    MEMORY_FILL => {
                        pos += scan_decode::<u8>(&bytes[pos..])?;
                    }
                    TABLE_INIT => {
                        pos += scan_decode::<ElementIdx>(&bytes[pos..])?;
                        pos += scan_decode::<TableIdx>(&bytes[pos..])?;
                    }
                    ELEM_DROP => {
                        pos += scan_decode::<ElementIdx>(&bytes[pos..])?;
                    }
                    TABLE_COPY => {
                        pos += scan_decode::<TableIdx>(&bytes[pos..])?;
                        pos += scan_decode::<TableIdx>(&bytes[pos..])?;
                    }
                    TABLE_GROW  | TABLE_SIZE | TABLE_FILL => {
                        pos += scan_decode::<TableIdx>(&bytes[pos..])?;
                    }
                    EXTENDED_RESERVED!() => {
                        return Err(());
                    }
                }
            }
            RESERVED!() => {
                return Err(());
            }
        }
    }
}

impl Decode for Expr {
    fn decode(bytes: &mut &[u8]) -> Result<Expr, ()> {
        let (end_pos, ending) = scan_expr(bytes, 0)?;
        if ending != crate::instr::bytes::END {
            return Err(());
        }
        let (expr, rest) = bytes.split_at(end_pos);
        *bytes = rest;
        Ok(Expr {
            instructions: Vec::from(expr),
        })
    }
}

#[derive(Decode)]
struct Locals {
    count: u32,
    typ: ValueType,
}
struct Code {
    locals: Vec<ValueType>,
    body: Expr,
}
impl Decode for Code {
    fn decode(bytes: &mut &[u8]) -> Result<Code, ()> {
        let code_len = u32::decode(bytes)? as usize;
        if code_len >= bytes.len() {
            return Err(());
        }
        let (mut code, rest) = bytes.split_at(code_len);
        *bytes = rest;

        let locals = Vec::<Locals>::decode(&mut code)?;
        let locals: Vec<_> = locals.into_iter().flat_map(|local| {
            std::iter::repeat(local.typ).take(local.count as usize)
        }).collect();
        let body = Decode::decode(&mut code)?;
        if !code.is_empty() {
            return Err(());
        }

        Ok(Code { locals, body })
    }
}

impl Decode for Module {
    fn decode(bytes: &mut &[u8]) -> Result<Module, ()> {
        const MAGIC: [u8; 4] = [0x00, 0x61, 0x73, 0x6d];
        if !bytes.starts_with(&MAGIC) {
            return Err(());
        }

        let version = {
            let version_bytes = bytes.get(4..8).ok_or(())?;
            let version_bytes = version_bytes.try_into().expect("slice must have length 4");
            *bytes = &bytes[8..];
            u32::from_le_bytes(version_bytes)
        };
        if version != 1 {
            return Err(());
        }

        skip_custom_sections(bytes)?;
        let types = decode_section(bytes, 1)?;
        skip_custom_sections(bytes)?;
        let imports = decode_section(bytes, 2)?;
        skip_custom_sections(bytes)?;
        let functions: Vec<TypeIdx> = decode_section(bytes, 3)?;
        skip_custom_sections(bytes)?;
        let tables = decode_section(bytes, 4)?;
        skip_custom_sections(bytes)?;
        let memories = decode_section(bytes, 5)?;
        skip_custom_sections(bytes)?;
        let globals = decode_section(bytes, 6)?;
        skip_custom_sections(bytes)?;
        let exports = decode_section(bytes, 7)?;
        skip_custom_sections(bytes)?;
        let start = decode_section(bytes, 8)?;
        skip_custom_sections(bytes)?;
        let elem = decode_section(bytes, 9)?;
        skip_custom_sections(bytes)?;
        let data_count: Option<u32> = decode_section(bytes, 12)?;
        skip_custom_sections(bytes)?;
        let code: Vec<Code> = decode_section(bytes, 10)?;
        skip_custom_sections(bytes)?;
        let data: Vec<DataSegment> = decode_section(bytes, 11)?;
        skip_custom_sections(bytes)?;

        if !bytes.is_empty() {
            return Err(());
        }

        if let Some(data_count) = data_count {
            if data_count as usize != data.len() {
                return Err(());
            }
        }

        if functions.len() != code.len() {
            return Err(());
        }
        let functions = functions.into_iter().zip(code)
            .map(|(typ, Code { locals, body })| {
                Function { typ, locals, body }
            })
            .collect();

        Ok(Module {
            types,
            functions,
            tables,
            memories,
            globals,
            elem,
            data,
            start,
            imports,
            exports,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn decode_exact<P: Decode>(mut bytes: &[u8]) -> Result<P, ()> {
        let result = P::decode(&mut bytes);
        if result.is_ok() {
            assert!(bytes.is_empty(), "bytes should be empty after parsing, actually: {:x?}", bytes);
        }
        result
    }

    #[test]
    fn decode_u32() {
        // Test all proper lengths
        assert_eq!(Ok(0x0), decode_exact::<u32>(b"\x00"));
        assert_eq!(Ok(0x0), decode_exact::<u32>(b"\x80\x00"));
        assert_eq!(Ok(0x0), decode_exact::<u32>(b"\x80\x80\x00"));
        assert_eq!(Ok(0x0), decode_exact::<u32>(b"\x80\x80\x80\x00"));
        assert_eq!(Ok(0x0), decode_exact::<u32>(b"\x80\x80\x80\x80\x00"));

        // Test non-zero in unused bits
        assert_eq!(Err(()), decode_exact::<u32>(b"\x80\x80\x80\x80\x10"));
        assert_eq!(Err(()), decode_exact::<u32>(b"\x80\x80\x80\x80\x20"));
        assert_eq!(Err(()), decode_exact::<u32>(b"\x80\x80\x80\x80\x40"));
        assert_eq!(Err(()), decode_exact::<u32>(b"\x80\x80\x80\x80\x80"));

        // Test improper length
        assert_eq!(Err(()), decode_exact::<u32>(b"\x80\x80\x80\x80\x80\x00"));

        // Test bit positions
        assert_eq!(Ok(0x12), decode_exact::<u32>(b"\x12"));
        assert_eq!(Ok(0x2293), decode_exact::<u32>(b"\x93\x45"));
        assert_eq!(Ok(0x1e1ba6), decode_exact::<u32>(b"\xa6\xb7\x78"));
        assert_eq!(Ok(0x19aed49), decode_exact::<u32>(b"\xc9\xda\xeb\x0c"));
        assert_eq!(Ok(0x03407c77d), decode_exact::<u32>(b"\xfd\x8e\x9f\xa0\x03"));

        // Test all bits set
        assert_eq!(Ok(0xffffffff), decode_exact::<u32>(b"\xff\xff\xff\xff\x0f"));
    }

    #[test]
    fn decode_i32() {
        // Test all proper lengths
        assert_eq!(Ok(0x0), decode_exact::<i32>(b"\x00"));
        assert_eq!(Ok(0x0), decode_exact::<i32>(b"\x80\x00"));
        assert_eq!(Ok(0x0), decode_exact::<i32>(b"\x80\x80\x00"));
        assert_eq!(Ok(0x0), decode_exact::<i32>(b"\x80\x80\x80\x00"));
        assert_eq!(Ok(0x0), decode_exact::<i32>(b"\x80\x80\x80\x80\x00"));

        // Test all sign extension lengths
        assert_eq!(Ok(-16), decode_exact::<i32>(b"\x70"));
        assert_eq!(Ok(-4096), decode_exact::<i32>(b"\x80\x60"));
        assert_eq!(Ok(-786432), decode_exact::<i32>(b"\x80\x80\x50"));
        assert_eq!(Ok(-134217728), decode_exact::<i32>(b"\x80\x80\x80\x40"));
        assert_eq!(Ok(-268435456), decode_exact::<i32>(b"\x80\x80\x80\x80\x0f"));

        // Test non-zero in unused bits
        assert_eq!(Err(()), decode_exact::<i32>(b"\x80\x80\x80\x80\x10"));
        assert_eq!(Err(()), decode_exact::<i32>(b"\x80\x80\x80\x80\x20"));
        assert_eq!(Err(()), decode_exact::<i32>(b"\x80\x80\x80\x80\x40"));
        assert_eq!(Err(()), decode_exact::<i32>(b"\x80\x80\x80\x80\x80"));

        // Test improper length
        assert_eq!(Err(()), decode_exact::<i32>(b"\x80\x80\x80\x80\x80\x00"));

        // Test bit positions
        assert_eq!(Ok(18), decode_exact::<i32>(b"\x12"));
        assert_eq!(Ok(-7533), decode_exact::<i32>(b"\x93\x45"));
        assert_eq!(Ok(-123994), decode_exact::<i32>(b"\xa6\xb7\x78"));
        assert_eq!(Ok(26930505), decode_exact::<i32>(b"\xc9\xda\xeb\x0c"));
        assert_eq!(Ok(872925053), decode_exact::<i32>(b"\xfd\x8e\x9f\xa0\x03"));

        // Test all bits set
        assert_eq!(Ok(-1), decode_exact::<i32>(b"\xff\xff\xff\xff\x0f"));
    }

    #[test]
    fn decode_vec() {
        // Test empty vector
        assert_eq!(Ok(vec![]), decode_exact::<Vec<u32>>(b"\x00"));
    }

    #[test]
    fn decode_str() {
        // Test empty string
        assert_eq!(Ok(String::new()), decode_exact::<String>(b"\x00"));
    }
}
