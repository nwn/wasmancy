use proc_macro::TokenStream;
use syn;
use quote::quote;

#[proc_macro_derive(Decode)]
pub fn derive_decode(input: TokenStream) -> TokenStream {
    let typ_def = syn::parse_macro_input!(input as syn::DeriveInput);

    let trait_path = quote!(Decode);
    let ident = typ_def.ident;
    let (impl_generics, type_generics, where_clause) = typ_def.generics.split_for_impl();

    let code = match typ_def.data {
        syn::Data::Struct(syn::DataStruct { fields, ..}) => {
            match fields {
                syn::Fields::Named(syn::FieldsNamed { named: fields, .. }) => {
                    let fields = fields.into_iter().map(|field| {
                        let ident = field.ident;
                        quote!(#ident: #trait_path::decode(bytes)?,)
                    });
                    let fields = fields.fold(quote!(), |prev, cur| quote!(#prev #cur));
                    quote! {
                        Self {
                            #fields
                        }
                    }
                }
                syn::Fields::Unnamed(syn::FieldsUnnamed { unnamed: fields, .. }) => {
                    let fields = fields.into_iter().map(|_field| {
                        quote!(#trait_path::decode(bytes)?,)
                    });
                    let fields = fields.fold(quote!(), |prev, cur| quote!(#prev #cur));
                    quote! {
                        Self(#fields)
                    }
                }
                syn::Fields::Unit => quote!(Self),
            }
        }
        syn::Data::Enum(syn::DataEnum { variants: _, .. }) => {
            panic!("Not yet able to derive `Decode` for `enum` types")
        }
        syn::Data::Union(_) => {
            panic!("Unable to derive `Decode` for `union` types")
        }
    };

    let output = quote! {
        impl #impl_generics #trait_path for #ident #type_generics #where_clause {
            fn decode(bytes: &mut &[u8]) -> std::result::Result<Self, ()> {
                Ok(#code)
            }
        }
    };
    output.into()
}
